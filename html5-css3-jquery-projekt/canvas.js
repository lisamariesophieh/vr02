(function(){

    function init(){
        var canvas = document.getElementsByTagName('canvas')[0];
        var c = canvas.getContext('2d');
        c.width  = window.innerWidth;
        c.height = window.innerHeight;

   (function(){

       var distance = 0;
       var velocity = 200; //200pixels/second
       var lastFrameRepaintTime = 0;


       function drawCar(){
           c.save();
           c.lineWidth = 3;
           c.strokeStyle = 'black';
           c.beginPath();
           c.moveTo(350, 260);
           c.lineTo(0, 260);
           c.lineTo(0, 200);
           c.lineTo(50, 200);
           c.lineTo(100, 150);
           c.lineTo(250, 150);
           c.lineTo(300, 200);
           c.lineTo(350, 200);
           c.stroke();
           c.beginPath();
           c.moveTo(350, 200);
           c.quadraticCurveTo(370, 230, 350, 260);
           c.stroke();
           c.beginPath();
           c.arc(90, 290, 30, 0, 2 * Math.PI,false);
           c.stroke();
           c.beginPath();
           c.arc(270, 290, 30, 0, 2 * Math.PI,false);
           c.stroke();
           c.restore();
       }
       function animate(time) {
           var frameGapTime = time - lastFrameRepaintTime;
           lastFrameRepaintTime = time;
           var translateX = velocity * (frameGapTime/1000);
           c.clearRect(-distance, 0, canvas.width, canvas.height);
           drawCar();
           c.translate(translateX,0);
           distance += translateX;
           if(distance < 1400){
           requestAnimationFrame(animate);
           }
       }


       lastFrameRepaintTime = window.performance.now();
       requestAnimationFrame(animate);
    }());
    }

//do function init once document is loaded
    window.addEventListener('load',init,false);

}());
